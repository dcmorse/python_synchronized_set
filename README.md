## synchronized_set

Install: `pip install synchronized-set`

Use: `from synchronized_set import SynchronizedSet`

Then just use as you would a notmal builtin `set`.

This is just a simple wrapper around the builtin `set` class
that allows safer access from multiple threads.
