# TODO: make this better
from synchronized_set import SynchronizedSet
import logging


if __name__ == '__main__':
    from threading import Thread
    import datetime as dt

    logging.getLogger().setLevel(logging.DEBUG)
    ITERATIONS = 100
    THREADS = 70
    times = {'synchronized': None, 'unsynchronized': None}
    for i in (True, False):
        times[f'{"" if i else "un"}synchronized'] = dt.datetime.utcnow()  # type: ignore
        ss1 = SynchronizedSet({1}, synchronized=i)
        ss2 = SynchronizedSet({10}, synchronized=i)
        logging.warning(f'ss1 = {ss1}')
        logging.warning(f'ss2 = {ss2}')

        addnum = lambda s, n: s.add(n)
        # trying to choke it
        for j in range(ITERATIONS):

            logging.warning(f'ss1 = {ss1}')
            logging.warning(f'ss2 = {ss2}')

            threads = {}
            for k in range(THREADS - 2):
                threads[f't{k}'] = Thread(target=(
                    lambda: addnum(
                        ss1 if (k % 2 == 0) else ss2,
                        frozenset(range(j + (k * 10), j + (10 * (k * 10)), j * (5 * k) + 1))
                    )
                ))

            for k in range(THREADS - 2, THREADS):
                threads[f't{k}'] = Thread(
                    target=(ss1 if (k % 2 == 0) else ss2).clear
                )

            for tname, t in threads.items():
                logging.warning(f'calling `{tname}.start()`')
                t.start()
                logging.warning(f'ss1 = {ss1}')
                logging.warning(f'ss2 = {ss2}')

            if j % 6 == 0:
                for tname, t in threads.items():
                    logging.warning(f'calling `{tname}.join()`')
                    t.join()
                    logging.warning(f'ss1 = {ss1}')
                    logging.warning(f'ss2 = {ss2}')

        times[f'{"" if i else "un"}synchronized'] = (
            dt.datetime.utcnow() - times[f'{"" if i else "un"}synchronized']  # type: ignore
        )

    print('\nsynchronized time =', times['synchronized'])
    print('unsynchronized time =', times['unsynchronized'])
    exit(0)
